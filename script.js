console.warn(`Hello World`);

function Character(name, level, atk, hp){
	this.Name = name;
	this.Level = level;
	this.Atk = atk;
	this.Hp = hp;

	this.Battle = function(target){
		console.warn(`${this.Name} attacked ${target.Name}`);
		target.Hp -= this.Atk;
		console.warn(`HP: ${target.Hp}`);

		if(target.Hp <= 0){
			target.Faint();
		}
	};

	this.Faint = function(){
		console.error(`${this.Name} has 0 hp`);
	};
}

let c1 = new Character(`Groudon`, 1, 5, 20);
let c2 = new Character(`Kyogre`, 1, 5, 20);

console.warn(c1);
console.warn(c2);

c1.Battle(c2);
c1.Battle(c2);
c1.Battle(c2);
c1.Battle(c2);